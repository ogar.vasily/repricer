package productAttributesCtrl

import (
	"gopkg.in/mgo.v2/bson"
	"repr/backend/models/productAttributes"
	"gopkg.in/gin-gonic/gin.v1"
	"net/http"
	"gopkg.in/validator.v2"
	"github.com/leebenson/conform"
)

type ProductAttrValuesAPI struct {
}

type optionValues struct {
	Attribute string
	Values    []bson.M
}

//Create product attributes (POST) /product-attributes-values/create
func (this *ProductAttrValuesAPI) CreateProductAttrValues(ctx *gin.Context) {
	values := optionValues{}

	if ctx.BindJSON(&values) == nil {
		conform.Strings(&values)
		var response []productAttributes.ProductAttributesValuesEntity

		for _, element := range values.Values {
			option := this.prepareOption(element, values.Attribute)

			if err := validator.Validate(&option); err != nil {
				errs := err.(validator.ErrorMap)
				ctx.JSON(http.StatusOK, errs)
			} else {
				if result, err := productAttributes.CreateAttributeValue(option); err != nil {
					ctx.JSON(http.StatusOK, err)
				} else {
					response = append(response, result)
				}
			}
		}
		ctx.JSON(http.StatusOK, response)
	}
}

func (this *ProductAttrValuesAPI) prepareOption(element bson.M, attrId string) productAttributes.ProductAttributesValuesEntity {
	id := element["id"].(string)
	var option productAttributes.ProductAttributesValuesEntity
	if id != "" {
		option = productAttributes.ProductAttributesValuesEntity{
			AttributeId: bson.ObjectIdHex(attrId),
			Option: element["option"].(string),
			Id: bson.ObjectIdHex(id),
		}
	} else {
		option = productAttributes.ProductAttributesValuesEntity{
			AttributeId: bson.ObjectIdHex(attrId),
			Option: element["option"].(string),
		}
	}

	return option
}

//Get product attributes values (GET) /product-attribute-values
func (this *ProductAttrValuesAPI) GetProductAttrValues(ctx *gin.Context) {
	attribute_id := ctx.Param("attribute_id")

	result, err := productAttributes.GetAllByAttributeId(attribute_id)
	if err != nil {
		ctx.JSON(http.StatusOK, err)
		return
	} else {
		ctx.JSON(http.StatusOK, result)
	}
}

func (this *ProductAttrValuesAPI) DeleteProductAttrValue(ctx *gin.Context) {
	id := ctx.Param("id")

	err := productAttributes.DeleteById(id)
	if err != nil {
		ctx.JSON(http.StatusOK, err)
		return
	}
}

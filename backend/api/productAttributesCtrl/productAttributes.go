package productAttributesCtrl

import (
	//"encoding/json"
	"fmt"
	"gopkg.in/gin-gonic/gin.v1"
	"net/http"
	"github.com/leebenson/conform"
	"repr/backend/models/productAttributes"
	"gopkg.in/validator.v2"
	//"repr/backend/helpers"
	"repr/backend/models"
)

type ProductAttrAPI struct {
}

// Get all product attributes (GET)
func (this *ProductAttrAPI) GetAllProductAttrs(ctx *gin.Context) {
	result, err := productAttributes.GetAllProductAttrs()

	if err != nil {
		ctx.JSON(http.StatusOK, models.Err("1"))
		return
	} else {
		ctx.JSON(http.StatusOK, gin.H{"data": &result})
	}
}

// Get product attributes by id (GET)
func (this ProductAttrAPI) GetProductAttrById(ctx *gin.Context) {
	result, err := productAttributes.GetProductAttrById(ctx.Param("id"))

	if err != nil {
		fmt.Println(err)
		ctx.JSON(http.StatusOK, models.Err("1"))
		return
	} else {
		fmt.Println(result)
		ctx.JSON(http.StatusOK, result)
	}
}

// Update product attributes (PUT) /product-attributes/update/:id
func (this ProductAttrAPI) UpdateProductAttrById(ctx *gin.Context) {
	// Get form data
	id := ctx.Param("id")
	//err := json.Unmarshal(helper.Sanitize(data), &attributes)

	// Update
	var attributes productAttributes.ProductAttributesEntity
	if ctx.BindJSON(&attributes) == nil {
		conform.Strings(&attributes)
		if err := productAttributes.UpdateProductAttrById(id, &attributes); err != nil {
			ctx.JSON(http.StatusOK, models.Err("5"))
		} else {
			ctx.JSON(http.StatusOK, gin.H{"response": true})
		}
	}
}

//Create product attributes (POST) /product-attributes/create
func (this ProductAttrAPI) CreateProductAttr(ctx *gin.Context) {
	//err := json.Unmarshal(helper.Sanitize(data), &attributes)

	var attributes productAttributes.ProductAttributesEntity
	if ctx.BindJSON(&attributes) == nil {
		conform.Strings(&attributes)
		if err := validator.Validate(&attributes); err != nil {
			// values not valid, deal with errors here
			fmt.Println("error")
			errs := err.(validator.ErrorMap)
			ctx.JSON(http.StatusOK, errs)
		} else {
			fmt.Println("no error")

			if err := productAttributes.CreateProductAttr(&attributes); err != nil {
				ctx.JSON(http.StatusOK, models.Err("5"))
				return
			} else {
				ctx.JSON(http.StatusOK, gin.H{"response": true})
			}
		}
	}
	// exp
}

// Delete product attributes by id (DELETE) /product-attributes/:id
func (this ProductAttrAPI) DeleteProductAttrById(ctx *gin.Context) {
	id := ctx.Param("id")

	if err := productAttributes.DeleteProductAttrById(id); err != nil {
		ctx.JSON(http.StatusOK, models.Err("1"))
		return
	} else {
		ctx.JSON(http.StatusOK, gin.H{"response": true})
	}
}

func (this ProductAttrAPI) GetProductColumns(ctx *gin.Context) {
	response, err := productAttributes.GetProductColumns()
	fmt.Println(response)

	if err != nil {
		ctx.JSON(http.StatusOK, err)
		return
	} else {
		ctx.JSON(http.StatusOK, response)
	}
}
func (this ProductAttrAPI) GetProductAttrAggregated(ctx *gin.Context) {
	response, err := productAttributes.GetAllProductAttrs()
	fmt.Println(response)

	if err != nil {
		ctx.JSON(http.StatusOK, err)
		return
	} else {
		ctx.JSON(http.StatusOK, response)
	}
}

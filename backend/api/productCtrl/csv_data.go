package productCtrl

import (
	"log"
	"repr/backend/models/product"
	"repr/backend/models/import"
	"gopkg.in/gin-gonic/gin.v1"
	"net/http"
)

func CsvUpdateProducts(ctx *gin.Context, importObject imports.ImportEntity) {
	err := product.CsvUpdatePullProducts(importObject)
	err = product.CsvUpdatePushProducts(importObject)
	if err != nil {
		ctx.JSON(http.StatusOK, gin.H{"error": err})
	} else {
		ctx.JSON(http.StatusOK, gin.H{"response": true})
	}
}

func CsvCreateProducts(ctx *gin.Context, importObject imports.ImportEntity) {
	err := product.CsvCreateProducts(importObject)
	log.Println("zashel")
	if err != nil {
		ctx.JSON(http.StatusOK, gin.H{"error": err})
	} else {
		ctx.JSON(http.StatusOK, gin.H{"response": true})
	}
}

func CsvDeleteProducts(ctx *gin.Context, importObject imports.ImportEntity) {
	err := product.CsvDeleteProducts(importObject)
	if err != nil {
		ctx.JSON(http.StatusOK, gin.H{"error": err})
	} else {
		ctx.JSON(http.StatusOK, gin.H{"response": true})
	}
}

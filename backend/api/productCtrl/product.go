package productCtrl

import (
	"github.com/leebenson/conform"
	"gopkg.in/mgo.v2/bson"
	"log"
	"repr/backend/helpers"
	"repr/backend/models"
	"time"
	"repr/backend/models/productAttributes"
	"repr/backend/models/product"
	"repr/backend/models/import"
	"gopkg.in/gin-gonic/gin.v1"
	"net/http"
	"strings"
)

type ProductAPI struct {
	*gin.Context
}

// Get all products (GET)
func (this *ProductAPI) GetAllProducts(ctx *gin.Context) {
	helper := new(helpers.Helpers)
	columns := this.getProductColumns()
	response := helper.DtProcessing(columns, "products", ctx)
	ctx.JSON(http.StatusOK, response)
}

// Get product attributes-columns
func (this *ProductAPI) getProductColumns() []string {
	colsData, _ := productAttributes.GetProductColumns()
	columns := []string{}
	for _, v := range colsData {
		//fmt.Println(v)
		//fmt.Println(v.Code)
		if v.System {
			columns = append(columns, v.Code)
		} else {
			columns = append(columns, "attributes." + v.Code)
		}
	}
	//log.Println(columns)
	return columns
}

// Get product by id (GET)
func (this *ProductAPI) GetProductById(ctx *gin.Context) {
	id := ctx.Param("id")
	result, err := product.GetProductById(id)
	if err != nil {
		ctx.JSON(http.StatusOK, gin.H{"error": err})
	} else {
		ctx.JSON(http.StatusOK, &result)
	}
}

// Update product (PUT) /products/update/:id
func (this *ProductAPI) UpdateProductById(ctx *gin.Context) {
	id := ctx.Param("id")
	var json product.ProductEntity
	//helper := new(helpers.Helpers)
	//cleanJson := helper.Sanitize(json)

	if ctx.BindJSON(&json) == nil {
		conform.Strings(&json)
		err := product.UpdateProductById(id, &json)
		if err != nil {
			ctx.JSON(http.StatusOK, models.Err("5"))
		} else {
			ctx.JSON(http.StatusOK, gin.H{"response": true})
		}
	}
}

// Create product (POST) /products/create
func (this *ProductAPI) CreateProduct(ctx *gin.Context) {
	var json product.ProductEntity
	//helper := new(helpers.Helpers)
	//cleanJson := helper.Sanitize(json)
	if ctx.BindJSON(&json) == nil {
		conform.Strings(&json)
		err := product.CreateProduct(&json)
		if err != nil {
			ctx.JSON(http.StatusOK, models.Err("5"))
		} else {
			ctx.JSON(http.StatusOK, gin.H{"response": true})
		}
	}
}

// Delete product by id (DELETE) /products/delete/:id
func (this *ProductAPI) DeleteProductById(ctx *gin.Context) {
	id := ctx.Param("id")
	err := product.DeleteProductById(id)
	if err != nil {
		ctx.JSON(http.StatusOK, models.Err("1"))
		return
	} else {
		ctx.JSON(http.StatusOK, gin.H{"response": true})
	}
}

func (this *ProductAPI) UpdateProducts(ctx *gin.Context) {
	id := ctx.Param("id")
	//fmt.Println(id)
	importObject, err := imports.GetById(id)
	if err != nil {
		log.Println(err)
		ctx.JSON(http.StatusOK, models.Err("5"))
	}
	//log.Println(importObject.UpdateType)
	updateTypes := strings.Split(importObject.UpdateType, "_")

	for _, v := range updateTypes {
		//log.Println(v)
		switch v {
		case "add":
			log.Println(v)
			CsvCreateProducts(ctx, importObject)
		case "delete":
			log.Println(v)
			CsvDeleteProducts(ctx, importObject)
		case "update":
			log.Println(v)
			CsvUpdateProducts(ctx, importObject)
		case "disable":
			log.Println(v)
			this.UpdateDisabled(ctx, importObject)
		}
	}
}

func (this *ProductAPI) UpdateDisabled(ctx *gin.Context, importObject imports.ImportEntity) {
	now := time.Now()
	past := now.Add(-1 * time.Hour)
	selector := bson.M{}
	if importObject.Warehouse == "" {
		selector = bson.M{
			"suppliers.supplier_id": importObject.Supplier,
			"updated_at":            bson.M{"$lte": past},
		}
	} else if importObject.Supplier == "" {
		selector = bson.M{
			"warehouses.warehouse_id": importObject.Warehouse,
			"updated_at":              bson.M{"$lte": past},
		}
	} else if importObject.Warehouse == "" && importObject.Supplier == "" {
		ctx.JSON(http.StatusOK, gin.H{"response": true, "error": "Please select warehouse or supplier or both"})
		return
	} else {
		selector = bson.M{
			"warehouses.warehouse_id": importObject.Warehouse,
			"suppliers.supplier_id":   importObject.Supplier,
			"updated_at":              bson.M{"$lte": past},
		}
	}
	change := bson.M{
		"$set": bson.M{
			"updated_at":       time.Now(),
			"status":           0,
			"warehouses.$.qty": 0,
			"suppliers.$.qty":  0,
		},
	}

	info, err := product.UpdateAll(selector, change)
	if err != nil {
		log.Panic(err)
	} else {
		ctx.JSON(http.StatusOK, gin.H{"response": true, "result": info})
	}
}

func (this *ProductAPI) DeleteOldProducts(ctx *gin.Context, importObject imports.ImportEntity) {
	now := time.Now()
	past := now.Add(-1 * time.Hour)
	selector := bson.M{}
	if importObject.Warehouse == "" {
		selector = bson.M{
			"suppliers.supplier_id": importObject.Supplier,
			"updated_at":            bson.M{"$gte": past},
		}
	} else if importObject.Supplier == "" {
		selector = bson.M{
			"warehouses.warehouse_id": importObject.Warehouse,
			"updated_at":              bson.M{"$gte": past},
		}
	} else if importObject.Warehouse == "" && importObject.Supplier == "" {
		ctx.JSON(http.StatusOK, gin.H{"response": true, "error": "Please select warehouse or supplier or both"})
		return
	} else {
		selector = bson.M{
			"warehouses.warehouse_id": importObject.Warehouse,
			"suppliers.supplier_id":   importObject.Supplier,
			"updated_at":              bson.M{"$gte": past},
		}
	}

	err := product.DeleteProducts(selector)
	if err != nil {
		ctx.JSON(http.StatusOK, gin.H{"error": err})
	} else {
		ctx.JSON(http.StatusOK, gin.H{"response": true})
	}
}

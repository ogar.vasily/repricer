package api

import (
	"gopkg.in/gin-gonic/gin.v1"
	"net/http"
	"repr/backend/models"
)

type SettingsAPI struct {
}

func (this SettingsAPI) GetSettingsByName(ctx *gin.Context) {
	settings := new(models.Settings)
	result, err := settings.GetSettingsByName(ctx.Param("name"))

	if err != nil {
		panic(err)
	}
	ctx.JSON(http.StatusOK, &result)
}

package api

import (
	"fmt"
	"github.com/leebenson/conform"
	"repr/backend/models"
	"strings"
	"strconv"
	"gopkg.in/gin-gonic/gin.v1"
	"net/http"
)

type WarehousesAPI struct {
}

// Get all suppliers (GET) /suppliers
func (this WarehousesAPI) GetAllWarehouses(ctx *gin.Context) {
	draw, _ := strconv.Atoi(ctx.Param("draw"))
	warehouse := new(models.Warehouse)
	result, err := warehouse.GetAll()

	if err != nil {
		ctx.JSON(http.StatusOK, models.Err("1"))
		return
	} else {
		response := models.DtResponse{
			Draw:            draw,
			RecordsTotal:    len(result),
			RecordsFiltered: len(result),
			Data:            result,
		}
		ctx.JSON(http.StatusOK, &response)
	}
}

// Get all suppliers (GET) /warehouses/fields/:fields
func (this WarehousesAPI) GetAllWarehousesFields(ctx *gin.Context) {
	fields := ctx.Param("fields")
	params := strings.Split(fields, "+")
	var selectedFields map[string]interface{}

	for _, param := range params {
		prepared := strings.TrimSpace(param)
		if prepared == "id" {
			selectedFields["_id"] = 1
		}
		selectedFields[prepared] = 1
	}

	fmt.Println(selectedFields)
	warehouse := new(models.Warehouse)
	result, err := warehouse.GetAllProjected(selectedFields)

	if err != nil {
		ctx.JSON(http.StatusOK, models.Err("1"))
		return
	} else {
		ctx.JSON(http.StatusOK, &result)
	}
}

// Get warehouse by id (GET) /warehouse/:id
func (this WarehousesAPI) GetWarehouseById(ctx *gin.Context) {
	id := ctx.Param("id")
	warehouse := new(models.Warehouse)
	response, err := warehouse.GetById(id)

	if err != nil {
		ctx.JSON(http.StatusOK, err)
		return
	} else {
		ctx.JSON(http.StatusOK, response)
	}
}

// Update warehouse (PUT) /suppliers/update/:id
func (this WarehousesAPI) UpdateWarehouseById(ctx *gin.Context) {
	id := ctx.Param("id")
	//err := json.Unmarshal(helper.Sanitize(data), &warehouse)
	var warehouse models.Warehouse

	if ctx.BindJSON(&warehouse) == nil {
		//fmt.Println(warehouse)
		conform.Strings(&warehouse)
		suppliers := new(models.Warehouse)
		response, err := suppliers.UpdateById(id, &warehouse)
		if err != nil {
			ctx.JSON(http.StatusOK, err)
			return
		} else {
			ctx.JSON(http.StatusOK, gin.H{"response": true, "result": response})
		}
	}
}

// Create warehouse (POST) /suppliers/create
func (this WarehousesAPI) CreateWarehouse(ctx *gin.Context) {
	//helper := new(helpers.Helpers)
	//err := json.Unmarshal(helper.Sanitize(data), &warehouse)

	var warehouse models.Warehouse

	if ctx.BindJSON(&warehouse) == nil {
		//fmt.Println(warehouse)
		conform.Strings(&warehouse)
		warehouses := new(models.Warehouse)
		if err := warehouses.Create(&warehouse); err != nil {
			ctx.JSON(http.StatusOK, models.Err("5"))
			return
		} else {
			ctx.JSON(http.StatusOK, gin.H{"response": true})
		}
	}
}

// Delete warehouse by id (DELETE) /imports/delete/:id
func (this WarehousesAPI) DeleteWarehouseById(ctx *gin.Context) {
	id := ctx.Param("id")
	warehouse := new(models.Warehouse)

	if err := warehouse.Delete(id); err != nil {
		ctx.JSON(http.StatusOK, models.Err("1"))
		return
	} else {
		ctx.JSON(http.StatusOK, gin.H{"response": true})
	}
}

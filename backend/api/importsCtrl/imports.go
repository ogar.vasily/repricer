package importsCtrl

import (
	"repr/backend/helpers"
	"repr/backend/models"
	"strconv"
	"repr/backend/models/import"
	"gopkg.in/gin-gonic/gin.v1"
	"net/http"
	"github.com/leebenson/conform"
)

type ImportsAPI struct {

}

// Get all product attributes (GET)
func (this *ImportsAPI) GetAllImports(ctx *gin.Context) {
	draw,_ := strconv.Atoi(ctx.Param("draw"))
	response, err := imports.GetAllByParametes()
	if err != nil {
		ctx.JSON(http.StatusOK, models.Err("1"))
		return
	} else {
		response := models.DtResponse{
			Draw:            draw,
			RecordsTotal:    len(response),
			RecordsFiltered: len(response),
			Data:            response,
		}
		ctx.JSON(http.StatusOK, &response)
	}
}

// Get product attributes by id (GET)
func (this *ImportsAPI) GetImportById(ctx *gin.Context) {
	id := ctx.Param("id")
	response, err := imports.GetById(id)
	if err != nil {
		ctx.JSON(http.StatusOK, err)
		return
	} else {
		ctx.JSON(http.StatusOK, response)
	}
}

// Update product attributes (PUT) /imports/update/:id
func (this *ImportsAPI) UpdateImportById(ctx *gin.Context) {
	id := ctx.Param("id")
	var import_settings imports.ImportEntity
	//helper := new(helpers.Helpers)
	//import_settings = helper.Sanitize(import_settings)

	if ctx.BindJSON(&import_settings) == nil {
		conform.Strings(&import_settings)
		//log.Println(import_settings);
		if import_settings.Warehouse == "" && import_settings.Supplier == "" {
			ctx.JSON(http.StatusOK, gin.H{"response": true, "error": "Please select warehouse or supplier or both"})
			return
		}

		response, err := imports.UpdateById(id, &import_settings)
		if err != nil {
			ctx.JSON(http.StatusOK, err)
			return
		} else {
			ctx.JSON(http.StatusOK, gin.H{"response": true, "result": response})
		}
	}

}

// Create product attributes (POST) /imports/create
func (this *ImportsAPI) CreateImport(ctx *gin.Context) {
	var import_settings imports.ImportEntity
	//helper := new(helpers.Helpers)
	//import_settings = helper.Sanitize(import_settings)
	if ctx.BindJSON(&import_settings) == nil {
		conform.Strings(&import_settings)
		err := imports.CreateImport(&import_settings)
		if err != nil {
			ctx.JSON(http.StatusOK, models.Err("5"))
		} else {
			ctx.JSON(http.StatusOK, gin.H{"response": true})
		}
	}
}

// Delete product attributes by id (DELETE) /imports/delete/:id
func (this *ImportsAPI) DeleteImportById(ctx *gin.Context) {
	id := ctx.Param("id")
	err := imports.DeleteImportById(id)
	if err != nil {
		ctx.JSON(http.StatusOK, models.Err("1"))
		return
	} else {
		ctx.JSON(http.StatusOK, gin.H{"response": true})
	}
}
func (this *ImportsAPI) GetCsvData(ctx *gin.Context) {
	path := ctx.PostForm("file")
	offset := ctx.PostForm("offset")
	headers := ctx.PostForm("headers")
	headersBool, _ := strconv.ParseBool(string(headers))
	//fmt.Println(string(path))
	//fmt.Println(strconv.Atoi(string(offset)))
	//fmt.Println(separator)
	//fmt.Println(headers)
	offsetInt, _ := strconv.Atoi(string(offset))
	headersMap := map[string]string{
		"Article":          "music_po_catalog_no",
		"Bar Code":         "barcode",
		"Artist":           "music_artist",
		"Title":            "name",
		"Label":            "record_label",
		"Dealer Price":     "cost",
		"Short Kop Name":   "music_format",
		"Number Of Pieces": "format_qty",
		"Available Stock":  "qty",
	}

	settings := helpers.CsvSettings{
		File:      string(path),
		Offset:    offsetInt,
		HeaderMap: headersMap,
		Headers:   headersBool,
	}

	helper := new(helpers.Helpers)
	data, err := helper.ParseCSV(settings)
	if err != nil {
		ctx.JSON(http.StatusOK, err)
		return
	} else {
		ctx.JSON(http.StatusOK, data)
	}
}

func (this *ImportsAPI) GetRomMap() map[string]string {
	RomMap := map[string]string{
		"Article" : "music_po_catalog_no",
		"Bar Code": "barcode",
		"Artist": "music_artist",
		"Title": "name",
		"Label": "record_label",
		"Dealer Price": "cost",
		"Short Kop Name": "music_format",
		"Number Of Pieces": "format_qty",
		"Available Stock": "qty",
	}
	return RomMap
}

func (this *ImportsAPI) GetProperMap() map[string]string {
	ProperMap := map[string]string{
		"CatNumber" : "music_po_catalog_no",
		"Barcode": "barcode",
		"Artist": "music_artist",
		"Title": "name",
		"Label": "record_label",
		"DealerPrice": "cost",
		"Format": "music_format",
		"Stock": "qty",
		"Supplier": "supplier",
	}
	return ProperMap
}

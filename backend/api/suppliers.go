package api

import (
	"fmt"
	"github.com/leebenson/conform"
	"gopkg.in/gin-gonic/gin.v1"
	"net/http"
	"repr/backend/models"
	"strings"
	"strconv"
)

type SuppliersAPI struct {
}

// Get all suppliers (GET) /suppliers
func (this *SuppliersAPI) GetAllSuppliers(ctx *gin.Context) {
	draw, _ := strconv.Atoi(ctx.Param("draw"))
	supplier := new(models.Supplier)
	result, err := supplier.GetAll()

	if err != nil {
		ctx.JSON(http.StatusOK, models.Err("1"))
		return
	} else {
		response := models.DtResponse{
			Draw:            draw,
			RecordsTotal:    len(result),
			RecordsFiltered: len(result),
			Data:            result,
		}
		ctx.JSON(http.StatusOK, &response)
	}
}

// Get supplier by id (GET) /suppliers/:id
func (this *SuppliersAPI) GetSupplierById(ctx *gin.Context) {
	id := ctx.Param("id")
	supplier := new(models.Supplier)
	response, err := supplier.GetById(id)
	if err != nil {
		ctx.JSON(http.StatusOK, err)
		return
	} else {
		ctx.JSON(http.StatusOK, response)
	}
}

// Update supplier (PUT) /suppliers/update/:id
func (this *SuppliersAPI) UpdateSupplierById(ctx *gin.Context) {
	id := ctx.Param("id")
	//helper := new(helpers.Helpers)
	//err := json.Unmarshal(helper.Sanitize(data), &supplier)

	var supplier models.Supplier
	if ctx.BindJSON(&supplier) == nil {
		fmt.Println(supplier)
		conform.Strings(&supplier)
		suppliers := new(models.Supplier)
		response, err := suppliers.UpdateById(id, &supplier)
		if err != nil {
			ctx.JSON(http.StatusOK, err)
			return
		} else {
			ctx.JSON(http.StatusOK, gin.H{"response": true, "result": response})
		}
	}
}

// Create supplier (POST) /suppliers/create
func (this *SuppliersAPI) CreateSupplier(ctx *gin.Context) {
	//helper := new(helpers.Helpers)
	//err := json.Unmarshal(helper.Sanitize(data), &supplier)

	var supplier models.Supplier

	if ctx.BindJSON(&supplier) == nil {
		fmt.Println(supplier)
		conform.Strings(&supplier)
		suppliers := new(models.Supplier)

		if err := suppliers.Create(&supplier); err != nil {
			ctx.JSON(http.StatusOK, models.Err("5"))
			return
		} else {
			ctx.JSON(http.StatusOK, gin.H{"response": true})
		}
	}
}

// Delete supplier by id (DELETE) /imports/delete/:id
func (this *SuppliersAPI) DeleteSupplierById(ctx *gin.Context) {
	id := ctx.Param("id")
	supplier := new(models.Supplier)

	if err := supplier.Delete(id); err != nil {
		ctx.JSON(http.StatusOK, models.Err("1"))
		return
	} else {
		ctx.JSON(http.StatusOK, gin.H{"response": true})
	}
}

// Get all suppliers (GET) /warehouses/fields/:fields
func (this *SuppliersAPI) GetAllSuppliersFields(ctx *gin.Context) {
	fields := ctx.Param("fields")
	params := strings.Split(fields, "+")
	var selectedFields map[string]interface{}

	for _, param := range params {
		prepared := strings.TrimSpace(param)
		if prepared == "id" {
			selectedFields["_id"] = 1
		}
		selectedFields[prepared] = 1
	}

	fmt.Println(selectedFields)
	supplier := new(models.Supplier)
	result, err := supplier.GetAllProjected(selectedFields)

	if err != nil {
		ctx.JSON(http.StatusOK, models.Err("1"))
		return
	} else {
		ctx.JSON(http.StatusOK, &result)
	}
}

package helpers

import (
	"reflect"
	"repr/backend/db"
	"os"
	"strconv"
	"strings"
	"gopkg.in/mgo.v2/bson"
	"path/filepath"
	"encoding/csv"
	"io"
	"fmt"
	"math"
	"gopkg.in/gin-gonic/gin.v1"
	"repr/backend/models"
)

type Helpers struct {
	//*iris.Context
}

func (this *Helpers) IsStructEmpty(object interface{}) bool {
	//First check normal definitions of empty
	if object == nil {
		return true
	} else if object == "" {
		return true
	} else if object == false {
		return true
	}

	//Then see if it's a struct
	if reflect.ValueOf(object).Kind() == reflect.Struct {
		// and create an empty copy of the struct object to compare against
		empty := reflect.New(reflect.TypeOf(object)).Elem().Interface()
		if reflect.DeepEqual(object, empty) {
			return true
		}
	}
	return false
}

/* CSV parsing */
type CsvSettings struct {
	File      string `json:"file" bson:"file"`
	Offset    int `json:"offset" bson:"offset"`
	Separator rune `json:"separator" bson:"separator"`
	Headers   bool `json:"headers" bson:"headers"`
	HeaderMap map[string]string `bson:"headersMap"`
	Limit     int `json:"limit" bson:"limit"`
}

func (this *Helpers) ParseCSV(settings CsvSettings) (map[int]map[string]string, error) {
	absPath, _ := filepath.Abs("../frontend/public/files/" + settings.File)
	f, err := os.Open(absPath)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	csvReader := csv.NewReader(f)
	csvReader.LazyQuotes = true
	data := map[int]map[string]string{}
	header := []string{}
	i := 0
	for {
		rows, err := csvReader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			return nil, err
		}
		//fmt.Printf("%+v\n", rows)
		if settings.Headers == true && i == settings.Offset {
			header = rows
		}
		if i > settings.Offset {
			//fmt.Printf("%+v\n", header)
			row := HeaderToData(rows, header, settings)
			data[i] = row
		}

		if i != 0 && i == settings.Limit {
			break
		}
		i++
	}
	fmt.Printf("%+v\n", settings.Limit)
	//fmt.Printf("%+v\n", data)
	return data, nil
}
func HeaderToData(row []string, header []string, settings CsvSettings) map[string]string {
	response := map[string]string{}
	for index := range row {
		if len(settings.HeaderMap) != 0 {
			var mappedValue string
			if len(header) == 0 {
				// If no header, then mapping by column number
				columnNumber := strconv.Itoa(index)
				mappedValue = settings.HeaderMap[columnNumber]
			} else {
				mappedValue = settings.HeaderMap[header[index]]
			}
			//fmt.Printf("%+v\n", len(header))
			//fmt.Printf("%+v\n", mappedValue)
			if mappedValue != "" {
				response[mappedValue] = row[index]
				//fmt.Printf("%+v\n", header[index])
				//fmt.Printf("%+v\n", row[index])
			}
		} else {
			response[header[index]] = row[index]
		}
	}
	return response
}

/* Datatables server side processing */
//type DtResponse struct {
//	Draw            int `json:"draw"`
//	RecordsTotal    int `json:"recordsTotal"`
//	RecordsFiltered int `json:"recordsFiltered"`
//	Data            []bson.M `json:"data"`
//}

func (this *Helpers) DtProcessing(fields []string, collection string, ctx *gin.Context) models.DtResponse {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	columnsCount, _ := strconv.Atoi(ctx.Query("columnsCount"))
	searchValue := ctx.Query("search[value]")
	order := ctx.Query("order[1][column]")
	types := ctx.Query("type")
	id := ctx.Query("id")

	/**
	 * Filters
	 */
	searchTermsAny := []bson.M{}
	searchTermsColumn := []bson.M{}
	sortFields := []string{}

	for i := 0; i < columnsCount; i++ {
		/**
		 * Individual column filtering
		 */
		ColSearchable := ctx.Query("columns[" + strconv.Itoa(i) + "][searchable]")
		ColSearch := ctx.Query("columns[" + strconv.Itoa(i) + "][search][value]")
		//ColRegex := ctx.URLParam("columns[" + strconv.Itoa(i) + "][search][regex]")

		if ColSearchable == "true" && ColSearch != "" {
			data := ctx.Query("columns[" + strconv.Itoa(i) + "][data]")
			//searchTermsColumn[data] = bson.RegEx{Pattern: strings.TrimSpace(ColSearch), Options: "i"}
			searchTermsColumn = append(searchTermsColumn, bson.M{data: bson.RegEx{Pattern: strings.TrimSpace(ColSearch), Options: "i"}})
		}

		/**
		 * Filtering by all columns
		 */
		if searchValue != "" && ColSearchable == "true" {
			data := ctx.Query("columns[" + strconv.Itoa(i) + "][data]")
			searchTermsAny = append(searchTermsAny, bson.M{data: bson.RegEx{Pattern: strings.TrimSpace(searchValue), Options: "i"}})
		}

		/**
		 * Ordering
		 */
		dir := ""

		if order != "" {
			colOrderable := ctx.Query("columns[" + strconv.Itoa(i) + "][orderable]")
			colOrderDir := ctx.Query("order[" + strconv.Itoa(i) + "][dir]")
			colOrder := ctx.Query("order[" + strconv.Itoa(i) + "][column]")
			if colOrderDir == "desc" {
				dir = "-"
			}
			if colOrderable == "true" && colOrder != "" {
				data := ctx.Query("columns[" + colOrder + "][data]")
				sortFields = append(sortFields, dir + data)

			}
		} else {
			colOrderDir := ctx.Query("order[0][dir]")
			colOrder := ctx.Query("order[0][column]")
			if colOrderDir == "desc" {
				dir = "-"
			}
			if i == 0 {
				data := ctx.Query("columns[" + colOrder + "][data]")
				sortFields = append(sortFields, dir + data)
			}
		}
	}

	find := bson.M{}
	if types == "warehouse" && id != "" {
		find = bson.M{"warehouses.id": bson.ObjectIdHex(id)}
	} else if types == "supplier" && id != "" {
		find = bson.M{"suppliers.id": bson.ObjectIdHex(id)}
	}
	if len(searchTermsColumn) != 0 && len(searchTermsAny) != 0 {
		find["$and"] = searchTermsColumn
		find["$or"] = searchTermsAny
	} else if len(searchTermsColumn) != 0 && len(searchTermsAny) == 0 {
		find["$and"] = searchTermsColumn
	} else if len(searchTermsColumn) == 0 && len(searchTermsAny) != 0 {
		find["$or"] = searchTermsAny
	}
	//log.Printf("%+v\n", find)

	/**
	 * Paging
	 */
	start, _ := strconv.Atoi(ctx.Query("start"))
	length, _ := strconv.Atoi(ctx.Query("length"))

	/**
	 * Fields Projection
	 */
	selectedFields := bson.M{}
	for _, v := range fields {
		selectedFields[v] = 1
	}
	//fmt.Printf("%+v\n", collection)
	//fmt.Printf("%+v\n", selectedFields)

	/**
	 * Query
	 */
	var result []interface{}
	err := Db.C(collection).Find(find).Select(selectedFields).Limit(length).Skip(start).Sort(sortFields...).All(&result)
	if err != nil {
		panic(err)
	}
	//fmt.Printf("%+v\n", result)

	/**
	 * Output
	 */
	draw, _ := strconv.Atoi(ctx.Query("draw"))
	total, _ := Db.C(collection).Find(find).Select(selectedFields).Count()
	response := models.DtResponse{
		Draw: draw,
		RecordsTotal: total,
		RecordsFiltered: total,
		Data: result,
	}
	//fmt.Printf("%+v\n", response)
	return response
}

func (this *Helpers) round(num float64) int {
	return int(num + math.Copysign(0.5, num))
}

func (this *Helpers) ToFixed(num float64, precision int) float64 {
	output := math.Pow(10, float64(precision))
	return float64(this.round(num * output)) / output
}
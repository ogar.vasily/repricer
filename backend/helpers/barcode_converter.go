package helpers
import (
	"fmt"
	"strings"
)

func (this *Helpers) ConvertToEAN( barcode string) string {
	barcodeTrimmed := strings.TrimSpace(barcode)
	switch len(barcodeTrimmed) {
	case 12:
		return "0" + barcodeTrimmed
	case 13:
		return barcodeTrimmed
	default:
		fmt.Println("Incorrect barcode lenght")
		return barcode
	}
}

func (this *Helpers) ConvertToUPC( barcode string) string {
	barcodeTrimmed := strings.TrimSpace(barcode)
	switch len(barcodeTrimmed) {
	case 12:
		return barcodeTrimmed
	case 13:
		return barcodeTrimmed[1:]
	default:
		fmt.Println("Incorrect barcode lenght")
		return barcode
	}
}

package helpers

import (
	"encoding/json"
	"fmt"
	"github.com/microcosm-cc/bluemonday"
	//"reflect"
	"strings"
)

func (this *Helpers) Sanitize(data []byte) []byte {
		p := bluemonday.UGCPolicy()
		sanitizedData := p.SanitizeBytes(data)
		s := string(sanitizedData)
		fixed := strings.Replace(s, "&#34;", "\"", -1)

		return []byte(fixed)
	}

//func (this *Helpers) SanitizeMap(data map[string]string) map[string]string {
func (this *Helpers) SanitizeMap(data map[string]string) {
	dataByte, err := json.Marshal(data)
	fmt.Println(err)
	p := bluemonday.UGCPolicy()
	sanitizedData := p.SanitizeBytes(dataByte)
	s := string(sanitizedData)
	fixed := strings.Replace(s, "&#34;", "\"", -1)
	fmt.Println("sanitize")
	fmt.Println(fixed)
	fmt.Println("/sanitize")
	//fixed, err = json.Unmarshal(fixed)

	//return map[string]string(fixed)
}

//func (this *Helpers) SanitizeStruct(v interface{}) interface{} {
//s := reflect.ValueOf(&v).Elem()
//fmt.Println(s)
//typeOfT := s.Type()
//for i := 0; i < s.NumField(); i++ {
//f := s.Field(i)
//fmt.Printf("%d: %s %s = %v\n", i,
//typeOfT.Field(i).Name, f.Type(), f.Interface())
//}
//return v
//}

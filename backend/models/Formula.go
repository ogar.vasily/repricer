package models

import "gopkg.in/mgo.v2/bson"

type Formula struct {
	Name          string `bson:"name"`
	//AccountId     bson.ObjectId `bson:"account_id"`
	//UserId        bson.ObjectId `bson:"user_id"`
	MarketplaceId bson.ObjectId `bson:"marketplace_id"`
	Country       []string `bson:"country"`
	Status        string `bson:"status"`
	Priority      uint8 `bson:"priority"`
	MinPrice      Price `bson:"min_price"`
	MaxPrice      Price `bson:"max_price"`
	Filter        []Filter `bson:"filter"`
}
type Price struct {
	Option    bson.ObjectId `bson: "option"` // Attribute id
	Operators string `bson:"operator"`       // (+-*/)
	InputType string `bson:"input_type"`     // Per Cent, Number
	Input     float32 `bson:"input"`         // value
}
type Filter struct {
	Option   string `bson:"option"`
	Operator string `bson:"operator"`
	Value    string `bson:"values"`
}
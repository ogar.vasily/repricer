package models
import (
	"repr/backend/db"
	"gopkg.in/mgo.v2/bson"
)

type Settings struct {
	Name string `conform:"trim" json:"name" bson:"name"`
	Data []Option `conform:"trim" json:"data" bson:"data"`
	//Countries         []Option `json:"countries" bson:"countries"`
	//AttrTypes         []Option `json:"attr_types" bson:"attr_types"`
	//Marketplaces      []Option `json:"marketplaces" bson:"marketplaces"`
	//ImportTypes       []Option `json:"import_types" bson:"import_types"`
	//ImportUpdateTypes []Option `json:"import_update_types" bson:"import_update_types"`
	//MathOperators     []Option `json:"math_operators" bson:"math_operators"`
	//FilterOperators   []Option `json:"filter_operators" bson:"filter_operators"`
	//Roles             map[string]string `json:"roles" bson:"roles"`
}
type Option struct {
	Name string `conform:"trim" json:"name" bson:"name"`
	Code string `conform:"trim" json:"code" bson:"code"`
}

func (this *Settings) GetSettingsByName(param string) (Settings, error) {

	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	result := Settings{}
	err := Db.C("settings").Find(bson.M{"name": param}).One(&result);
	return result, err
}

package imports

import (
	"gopkg.in/mgo.v2/bson"
)
/* Import settings */
type ImportEntity struct {
	Id            bson.ObjectId `conform:"trim" json:"id" bson:"_id,omitempty"`
	Name          string `conform:"trim" json:"name" bson:"name"`
	Type          string `conform:"trim" json:"type" bson:"type"`
	FileType      string `conform:"trim" json:"file_type" bson:"file_type"`
	UpdateType    string `conform:"trim" json:"update_type" bson:"update_type"`
	Schedule      string `conform:"trim" json:"schedule" bson:"schedule"` // Save as default Cron * * * * * (m h dom m y dow)
	Values        map[string]string `conform:"trim" json:"values" bson:"values"`
	Status        bool `conform:"trim" json:"status" bson:"status"`
	Currency      string `conform:"trim" json:"currency" bson:"currency"`
	Mapping       []AttrMap `json:"mapping" bson:"mapping"`
	Supplier      bson.ObjectId `conform:"trim" json:"supplier,omitempty" bson:"supplier,omitempty"`
	Warehouse     bson.ObjectId `conform:"trim" json:"warehouse,omitempty" bson:"warehouse,omitempty"`
	QtyLimit      float64 `json:"qty_limit,string" bson:"qty_limit"`
	PriceModifier float64 `json:"price_modifier,string" bson:"price_modifier"`
}
/* Attribute map */
type AttrMap struct {
	ImportAttr  string `conform:"trim" bson:"import_attr"`  //attribute from import
	ProductAttr string `conform:"trim" bson:"product_attr"` //attribute in our system, created before import by user
}

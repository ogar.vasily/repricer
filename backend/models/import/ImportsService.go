package imports

import (
	"repr/backend/db"
	"gopkg.in/mgo.v2/bson"
)

func GetById(id string) (ImportEntity, error) {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	result := ImportEntity{}
	if err := Db.C("imports").Find(bson.M{"_id": bson.ObjectIdHex(id)}).One(&result); err != nil {
		return result, err
	} else {
		return result, nil
	}
}
func UpdateById(id string, data *ImportEntity) (ImportEntity, error) {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()
	//log.Println(data)
	// Update
	result := ImportEntity{}
	if err := Db.C("imports").Update(bson.M{"_id": bson.ObjectIdHex(id)}, &data); err != nil {
		return result, err
	} else {
		return result, nil
	}
}

// Get all product attributes (GET)
func GetAllImports() ([]ImportEntity, error) {

	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	result := []ImportEntity{}

	err := Db.C("imports").Find(nil).All(&result)
	return result, err
}

// Create product attributes (POST) /imports/create
func CreateImport(imports *ImportEntity) error {

	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	return Db.C("imports").Insert(&imports)
}

// Delete product attributes by id (DELETE) /imports/delete/:id
func DeleteImportById(id string) error {

	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	return Db.C("imports").Remove(bson.M{"_id": bson.ObjectIdHex(id)})
}

func GetAllByParametes() ([]interface{}, error) {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	warehouseLookup := bson.M{"$lookup": bson.M{
		"from": "warehouses",
		"localField": "warehouse",
		"foreignField": "_id",
		"as": "warehouse_doc",
	}}
	supplierLookup := bson.M{"$lookup": bson.M{
		"from": "suppliers",
		"localField": "supplier",
		"foreignField": "_id",
		"as": "supplier_doc",
	}}
	var pipeline []bson.M

	pipeline = []bson.M{warehouseLookup, supplierLookup}

	var result []interface{}
	cursor := Db.C("imports").Pipe(pipeline).Iter()
	err := cursor.All(&result)
	if err != nil {
		return result, err
	} else {
		return result, nil
	}
}


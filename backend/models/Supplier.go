package models

import (
	"gopkg.in/mgo.v2/bson"
	"repr/backend/db"
)

type Supplier struct {
	Id             bson.ObjectId `conform:"trim" json:"id" bson:"_id,omitempty"`
	CompanyName string `conform:"trim" json:"name" bson:"name"`
	Address     Address `conform:"trim" json:"address" bson:"address"`
	Email       string `conform:"trim" json:"email" bson:"email,omitempty"`
	Phone       string `conform:"trim" json:"phone" bson:"phone,omitempty"`
	Fax         string `conform:"trim" json:"fax" bson:"fax"`
	VAT         string `conform:"trim" json:"vat_number" bson:"vat_number"`
	ContactPerson  Person `json:"contact_person" bson:"contact_person"`
	ShipmentMethod string `conform:"trim" json:"shipment_method" bson:"shipment_method"`
	PaymentMethod  string `conform:"trim" json:"payment_method" bson:"payment_method"`
	MinOrderAmount MinOrderAmount `json:"min_order_amount" bson:"min_order_amount"`
	Status         bool `conform:"trim" json:"status" bson:"status"`
}

type Address struct {
	Street   string `conform:"trim" json:"street" bson:"street"`
	City     string `conform:"trim" json:"city" bson:"city"`
	State    string `conform:"trim" json:"state" bson:"state"`
	Country  string `conform:"trim" json:"country,omitempty" bson:"country,omitempty"`
	PostCode string `conform:"trim" json:"post_code" bson:"post_code"`
}
type Person struct {
	Name       string `conform:"trim" json:"name" bson:"name"`
	Surname    string `conform:"trim" json:"surname" bson:"surname"`
	MiddleName string `conform:"trim" json:"middle_name" bson:"middle_name"`
	Email      string `conform:"trim" json:"email" bson:"email"`
	Phone      string `conform:"trim" json:"phone,omitempty" bson:"phone,omitempty"`
}

type MinOrderAmount struct {
	Qty  int `conform:"trim" json:"qty,string" bson:"qty"`
	Type string `conform:"trim" json:"type" bson:"type"`
}

func (this *Supplier) GetAll() ([]interface{}, error) {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	var result []interface{}
	if err := Db.C("suppliers").Find(nil).All(&result); err != nil {
		return nil, err
	} else {
		return result, nil
	}
}

func (this *Supplier) GetById(id string) (Supplier, error) {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	result := Supplier{}
	if err := Db.C("suppliers").Find(bson.M{"_id": bson.ObjectIdHex(id)}).One(&result); err != nil {
		return result, err
	} else {
		return result, nil
	}
}

func (this *Supplier) UpdateById(id string, data interface{}) (Supplier, error) {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	// Update
	result := Supplier{}
	if err := Db.C("suppliers").Update(bson.M{"_id": bson.ObjectIdHex(id)}, &data); err != nil {
		return result, err
	} else {
		return result, nil
	}
}

func (this *Supplier) Create(supplier *Supplier) error {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	if err := Db.C("suppliers").Insert(supplier); err != nil {
		return err
	} else {
		return nil
	}
}

func (this *Supplier) Delete(id string) error {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	if err := Db.C("suppliers").Remove(bson.M{"_id": bson.ObjectIdHex(id)}); err != nil {
		return err
	} else {
		return nil
	}
}

func (this *Supplier) GetAllProjected(fields bson.M) ([]bson.M, error) {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	var result []bson.M
	if err := Db.C("suppliers").Find(nil).Select(fields).All(&result); err != nil {
		return nil, err
	} else {
		return result, nil
	}
}

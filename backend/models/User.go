package models

import (
	"repr/backend/db"
	"gopkg.in/mgo.v2/bson"
	//"encoding/json"
)

type User struct {
	Name     string `conform:"trim" bson:"name"`
	Surname  string `conform:"trim" bson:"surname"`
	Email    string `conform:"trim" bson:"email"`
	Role     string `conform:"trim" bson:"role"`
	Password string `conform:"trim" bson:"password"`
	Status   bool `conform:"trim" bson:"status"`
}

// Get all users (GET)
func (this *User) GetAllUsers() ([]User, error) {

	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	result := []User{}
	err := Db.C("users").Find(nil).All(&result)
	return result, err
}

// Get user by id (GET)
func (this *User) GetUserById(id string) (User, error) {

	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	result := User{}
	err := Db.C("users").Find(bson.M{"id": id}).One(&result);
	return result, err
}

func (this *User) UpdateUserById(id bson.ObjectId, usr *User) error {

	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	return  Db.C("users").Update(id,  &usr);
}

// Create user (POST) /users/:param1
func (this *User) CreateUser(usr *User) error {

	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	// Insert
	return Db.C("users").Insert(&usr)
}

// Delete user by id (DELETE) /users/:id
func (this *User) DeleteUserById(id string) error{
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	return Db.C("users").Remove(bson.M{"id": id})
}

package models

//import go libs
import (
	"time"
	"gopkg.in/mgo.v2/bson"
)

type Account struct {
	Id           bson.ObjectId `conform:"trim" json:"id" bson:"_id,omitempty"`
	Name         string `conform:"trim" json:"name" bson:"name"`
	Status       bool `conform:"trim" bson:"status"`
	Slug         string `conform:"trim" bson:"slug"`
	CreatedAt    time.Time `conform:"trim" bson:"created_at"`
	Users        []User `bson:"users"`
	Marketplaces []Marketplace `bson:"marketplaces"`
	//ImportSets   []Import `bson:"import_sets"`
	//ProductAttr  []ProductAttributes `bson:"product_attrs"`
}

type Marketplace struct {
	Id     bson.ObjectId `conform:"trim" bson: "_id"`
	Values map[string]string `conform:"trim" bson:"values"`
}



///* Type of account */
//type AccType struct {
//	Type       string `bson:"account_type"` // Personal or company
//}

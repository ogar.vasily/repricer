package models

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)
/* History of imports */
type History struct {
	ImportDate time.Time `bson:"import_date"`
	UserId     bson.ObjectId `bson:"user_id"`
	ImportType string `bson:"import_type"`
}
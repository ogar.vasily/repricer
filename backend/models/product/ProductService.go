package product

import (
	"gopkg.in/mgo.v2/bson"
	"repr/backend/db"
	"fmt"
)

// Get product by id
func GetProductById(id string) (interface{}, error) {
	fmt.Printf(id)
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	//result := Product{}
	//err := Db.C("products").Find(bson.M{"_id": bson.ObjectIdHex(id)}).One(&result);

	p1 := bson.M{"$match":bson.M{"_id": bson.ObjectIdHex(id)}}

	p2 := bson.M{"$unwind":"$warehouses"}

	p3 := bson.M{
		"$lookup":bson.M{
			"from": "warehouses",
			"localField": "warehouses.id",
			"foreignField": "_id",
			"as": "warehouseName"}}

	//p4 := bson.M{"$unwind":"$warehouseName"}
	//
	//p5 := bson.M{"$group":bson.M{
	//	//"_id": "$_id",
	//	//"barcode":bson.M{"$first": "$barcode"},
	//	//"updated_at": bson.M{"$first": "$updated_at"},
	//	//"created_at": bson.M{"$first": "$created_at"},
	//	//"name": bson.M{"$first": "$name"},
	//	//"status": bson.M{"$first": "$status"},
	//	//"attributes": bson.M{"$first": "$attributes"},
	//	//"suppliers": bson.M{"$first": "$suppliers"},
	//	"warehouses": bson.M{"$push":bson.M{
	//		"name":"$warehouseName.name",
	//		"qty":"$warehouses.qty",
	//		"warehouse_id":"$warehouses.id",
	//		"sku":"$warehouses.sku",
	//		"cost":"$warehouses.cost"}}}}
	//
	p6 := bson.M{"$unwind": "$suppliers"}
	p7 := bson.M{
		"$lookup":bson.M{
			"from": "suppliers",
			"localField": "suppliers.id",
			"foreignField": "_id",
			"as": "supplierName"}}

	//p8 := bson.M{"$unwind":"$supplierName"}
	//p9 := bson.M{"$group": bson.M{
	//	//"_id": "$_id",
	//	//"barcode": bson.M{"$first": "$barcode"},
	//	//"updated_at": bson.M{"$first": "$updated_at"},
	//	//"created_at": bson.M{"$first": "$created_at"},
	//	//"name": bson.M{"$first": "$name"},
	//	//"status": bson.M{"$first": "$status"},
	//	//"attributes": bson.M{"$first": "$attributes"},
	//	//"warehouses": bson.M{"$first": "$warehouses"},
	//	"suppliers": bson.M{"$push":bson.M{
	//		"name":"$supplierName.name",
	//		"qty":"$suppliers.qty",
	//		"supplier_id":"$suppliers.id",
	//		"sku":"$suppliers.sku",
	//		"cost":"$suppliers.cost"}}},
	//}
	p11 := bson.M{"$project": bson.M{
		"_id": 0,
		"document": "$$ROOT",
	}}
	//p10 := bson.M{"$unwind":"$attributes"}
	//p11 := bson.M{
	//	"$lookup":bson.M{
	//		"from": "attributes",
	//		"localField": "suppliers.id",
	//		"foreignField": "_id",
	//		"as": "supplierName"}}

	var pipeline []bson.M
	pipeline = []bson.M{p1, p2, p3, p6, p7, p11}

	result := []bson.M{}
	cursor := Db.C("products").Pipe(pipeline).Iter()
	err := cursor.All(&result)
	fmt.Println("test")
	fmt.Println(result)
	fmt.Println("/test")

	if err != nil {
		return nil, err
	} else {
		return result, nil
	}
}

// Update product by id
func
UpdateProductById(id string, product *ProductEntity) error {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	err := Db.C("products").Update(bson.M{"_id": bson.ObjectIdHex(id)}, &product)
	if err != nil {
		return err
	}
	return nil
}

// Update all producst
func
UpdateAll(selector bson.M, update interface{}) (interface{}, error) {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	// Update
	info, err := Db.C("products").UpdateAll(selector, update);
	if err != nil {
		return nil, err
	} else {
		return info, err
	}
}

// Create product
func CreateProduct(product *ProductEntity) error {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	err := Db.C("products").Insert(&product)
	if err != nil {
		return err
	}
	return nil
}

// Delete product by id
func DeleteProductById(id string) error {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	err := Db.C("products").Remove(bson.M{"_id": bson.ObjectIdHex(id)})
	if err != nil {
		return err
	}
	return err
}

// Delete all products
func DeleteProducts(selector bson.M) error {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	err := Db.C("products").Remove(selector)
	if err != nil {
		return err
	}
	return err
}

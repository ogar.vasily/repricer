package product

import (
	"encoding/csv"
	"errors"
	"github.com/microcosm-cc/bluemonday"
	"github.com/shopspring/decimal"
	"gopkg.in/mgo.v2/bson"
	"io"
	"log"
	"os"
	"path/filepath"
	"repr/backend/db"
	"repr/backend/helpers"
	"repr/backend/models"
	"repr/backend/models/import"
	"repr/backend/api/importsCtrl"
	"time"
	"strings"
)

type CsvImport struct {

}

// Create product (POST) /products/create
func CsvCreateProducts(importObject imports.ImportEntity) error {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	//fmt.Printf("%+v\n", rows)
	absPath, _ := filepath.Abs(importObject.Values["url_address"])
	f, err := os.Open(absPath)
	if err != nil {
		log.Printf("File open error: %s", err)
		return err
	}
	defer f.Close()

	csvReader := csv.NewReader(f)
	csvReader.LazyQuotes = true

	warehouseObj := new(models.Warehouse)
	warehouse, err := warehouseObj.GetById(importObject.Warehouse.Hex())
	if err != nil {
		return err
	}

	Import := new(importsCtrl.ImportsAPI)
	headersMap := map[string]string{}
	if warehouse.PrefixSuffixText == "ROM-" || warehouse.PrefixSuffixText == "fdsa-" {
		headersMap = Import.GetRomMap()
	} else if warehouse.PrefixSuffixText == "PM-" {
		headersMap = Import.GetProperMap()
	} else {
		errors.New("Please select warehouse")
		return err
	}

	//supplierObj := new(Supplier)
	//supplier, err := supplierObj.GetById(importObject.Supplier)
	//if err != nil {
	//	return err
	//}
	settings := helpers.CsvSettings{
		Offset:    1,
		HeaderMap: headersMap,
		Headers:   true,
	}

	header := []string{}
	i := 0
	n := 0
	b := Db.C("products").Bulk()
	b.Unordered()
	for {
		rows, err := csvReader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Printf("File error: %s", err)
			return err
		}
		//log.Printf("%+v\n", rows)
		if i == 0 {
			header = rows
		}
		if i > 1 {
			//fmt.Printf("%+v\n", header)
			row := helpers.HeaderToData(rows, header, settings)
			var prefix string
			if warehouse.PrefixSuffixText == "PM-" && row["supplier"] == "Essential" {
				prefix = "EM-"
			} else {
				prefix = warehouse.PrefixSuffixText
			}

			//fmt.Printf("%+v\n", row)
			selector := bson.M{
				"barcode":       sanitizeAndTrim(row["barcode"]),
				"warehouses.id": importObject.Warehouse,
				"suppliers.id":  importObject.Supplier,
			}

			qty := modifyQty(sanitizeAndTrim(row["qty"]), importObject.QtyLimit)
			cost := modifyPrice(sanitizeAndTrim(row["cost"]), importObject.PriceModifier)
			warehouses := []bson.M{}
			warehouses = append(warehouses, bson.M{
				"id":  importObject.Warehouse,
				"qty": qty,
				"cost": bson.M{
					"price":    cost,
					"currency": importObject.Currency,
				},
				"sku": prefix + sanitizeAndTrim(row["music_po_catalog_no"]),
			})
			suppliers := []bson.M{}
			suppliers = append(suppliers, bson.M{
				"id":  importObject.Supplier,
				"qty": row["qty"],
				"cost": bson.M{
					"price":    row["cost"],
					"currency": importObject.Currency,
				},
				"sku": sanitizeAndTrim(row["music_po_catalog_no"]),
			})
			update := bson.M{
				"$setOnInsert": bson.M{
					"status":     1,
					"created_at": time.Now(),
					"updated_at": time.Now(),
					"name":       sanitizeAndTrim(row["name"]),
					"attributes.music_artist": sanitizeAndTrim(row["music_artist"]),
					"attributes.music_format": sanitizeAndTrim(row["music_format"]),
					"attributes.record_label": sanitizeAndTrim(row["record_label"]),
					"attributes.format_qty":   sanitizeAndTrim(row["format_qty"]),
					"warehouses":              warehouses,
					"suppliers":               suppliers,
				},
			}
			log.Println(selector)
			log.Println(update)
			b.Upsert(selector, update)
			n++
			if n == 99 {
				_, err = b.Run()
				if err != nil {
					log.Printf("bulk.Run:%s", err)
					return err
				}
				b = Db.C("products").Bulk()
				b.Unordered()
				n = 0
			}
		}
		i++
	}

	if n > 0 {
		_, err := b.Run()
		if err != nil {
			log.Printf("bulk.Run:%s", err)
			return err
		}
	}
	return nil
}

// Create product (POST) /products/create
func CsvUpdatePullProducts(importObject imports.ImportEntity) error {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	absPath, _ := filepath.Abs(importObject.Values["url_address"])
	f, err := os.Open(absPath)
	if err != nil {
		log.Printf("Open file: %s", err)
		return err
	}
	defer f.Close()

	csvReader := csv.NewReader(f)
	csvReader.LazyQuotes = true

	warehouseObj := new(models.Warehouse)
	warehouse, err := warehouseObj.GetById(importObject.Warehouse.Hex())
	if err != nil {
		return err
	}
	Import := new(importsCtrl.ImportsAPI)
	headersMap := map[string]string{}
	if warehouse.PrefixSuffixText == "ROM-" || warehouse.PrefixSuffixText == "fdsa-" {
		headersMap = Import.GetRomMap()
	} else if warehouse.PrefixSuffixText == "PM-" {
		headersMap = Import.GetProperMap()
	} else {
		errors.New("Please select warehouse")
		return err
	}
	settings := helpers.CsvSettings{
		Offset:    1,
		HeaderMap: headersMap,
		Headers:   true,
	}
	header := []string{}
	i := 0
	n := 0
	b := Db.C("products").Bulk()
	b.Unordered()
	for {
		rows, err := csvReader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Printf("File error: %s", err)
			return err
		}
		//fmt.Printf("%+v\n", rows)
		if i == 0 {
			header = rows
		}
		if i > settings.Offset {
			//fmt.Printf("%+v\n", header)
			row := helpers.HeaderToData(rows, header, settings)
			//fmt.Printf("%+v\n", row)

			selector := bson.M{}
			if importObject.Warehouse == "" {
				selector = bson.M{
					"suppliers." + importObject.Supplier.Hex(): bson.M{"$exists": true},
					"barcode": sanitizeAndTrim(row["barcode"]),
				}
			} else if importObject.Supplier == "" {
				selector = bson.M{
					"warehouses." + importObject.Warehouse.Hex(): bson.M{"$exists": true},
					"barcode": sanitizeAndTrim(row["barcode"]),
				}
			} else if importObject.Warehouse == "" && importObject.Supplier == "" {
				err = errors.New("Please select warehouse or supplier or both")
				return err
			} else {
				selector = bson.M{
					"warehouses.id": importObject.Warehouse,
					"suppliers.id":  importObject.Supplier,
					"barcode":       sanitizeAndTrim(row["barcode"]),
				}
			}

			update := bson.M{
				"$pull": bson.M{
					"warehouses": bson.M{"id": importObject.Warehouse},
					"suppliers":  bson.M{"id": importObject.Supplier},
				},
			}
			log.Println(selector)
			log.Println(update)
			b.Update(selector, update)
			n++
			if n == 99 {
				_, err = b.Run()
				if err != nil {
					log.Printf("bulk.Run: %s", err)
					return err
				}
				b = Db.C("products").Bulk()
				b.Unordered()
				n = 0
			}
		}
		i++
	}

	if n > 0 {
		_, err := b.Run()
		if err != nil {
			log.Printf("bulk.Run: %s", err)
			return err
		}
	}
	return nil
}

// Create product (POST) /products/create
func CsvUpdatePushProducts(importObject imports.ImportEntity) error {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	absPath, _ := filepath.Abs(importObject.Values["url_address"])
	f, err := os.Open(absPath)
	if err != nil {
		log.Printf("Open file: %s", err)
		return err
	}
	defer f.Close()

	csvReader := csv.NewReader(f)
	csvReader.LazyQuotes = true

	warehouseObj := new(models.Warehouse)
	warehouse, err := warehouseObj.GetById(importObject.Warehouse.Hex())
	if err != nil {
		return err
	}
	Import := new(importsCtrl.ImportsAPI)
	headersMap := map[string]string{}
	if warehouse.PrefixSuffixText == "ROM-" || warehouse.PrefixSuffixText == "fdsa-" {
		headersMap = Import.GetRomMap()
	} else if warehouse.PrefixSuffixText == "PM-" {
		headersMap = Import.GetProperMap()
	} else {
		errors.New("Please select warehouse")
		return err
	}
	settings := helpers.CsvSettings{
		Offset:    1,
		HeaderMap: headersMap,
		Headers:   true,
	}
	header := []string{}
	i := 0
	n := 0
	b := Db.C("products").Bulk()
	b.Unordered()
	for {
		rows, err := csvReader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Printf("File error: %s", err)
			return err
		}
		//fmt.Printf("%+v\n", rows)
		if i == 0 {
			header = rows
		}
		if i > settings.Offset {
			//fmt.Printf("%+v\n", header)
			row := helpers.HeaderToData(rows, header, settings)
			var prefix string
			if warehouse.PrefixSuffixText == "PM-" && row["supplier"] == "Essential" {
				prefix = "EM-"
			} else {
				prefix = warehouse.PrefixSuffixText
			}
			//fmt.Printf("%+v\n", row)

			selector := bson.M{}
			if importObject.Warehouse == "" {
				selector = bson.M{
					"suppliers.id": importObject.Supplier,
					"barcode":      sanitizeAndTrim(row["barcode"]),
				}
			} else if importObject.Supplier == "" {
				selector = bson.M{
					"warehouses.id": importObject.Warehouse,
					"barcode":       sanitizeAndTrim(row["barcode"]),
				}
			} else if importObject.Warehouse == "" && importObject.Supplier == "" {
				err = errors.New("Please select warehouse or supplier or both")
				return err
			} else {
				selector = bson.M{
					"barcode": sanitizeAndTrim(row["barcode"]),
				}
			}
			qty := modifyQty(sanitizeAndTrim(row["qty"]), importObject.QtyLimit)
			cost := modifyPrice(sanitizeAndTrim(row["cost"]), importObject.PriceModifier)

			update := bson.M{
				"$push": bson.M{
					"warehouses": bson.M{
						"id":  importObject.Warehouse,
						"qty": qty,
						"cost": bson.M{
							"price":    cost,
							"currency": importObject.Currency,
						},
						"sku": prefix + sanitizeAndTrim(row["music_po_catalog_no"]),
					},
					"suppliers": bson.M{
						"id":  importObject.Supplier,
						"qty": sanitizeAndTrim(row["qty"]),
						"cost": bson.M{
							"price":    sanitizeAndTrim(row["cost"]),
							"currency": importObject.Currency,
						},
						"sku": sanitizeAndTrim(row["music_po_catalog_no"]),
					},
				},
				"$set": bson.M{
					"updated_at": time.Now(),
				},
			}
			log.Println(selector)
			log.Println(update)
			b.Update(selector, update)
			n++
			if n == 99 {
				_, err = b.Run()
				if err != nil {
					log.Printf("bulk.Run: %s", err)
					return err
				}
				b = Db.C("products").Bulk()
				b.Unordered()
				n = 0
			}
		}
		i++
	}

	if n > 0 {
		_, err := b.Run()
		if err != nil {
			log.Printf("bulk.Run: %s", err)
			return err
		}
	}
	return nil
}

// Create product (POST) /products/create
func CsvDeleteProducts(importObject imports.ImportEntity) error {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	absPath, _ := filepath.Abs(importObject.Values["url_address"])
	f, err := os.Open(absPath)
	if err != nil {
		log.Printf("Open file: %s", err)
		return err
	}
	defer f.Close()

	csvReader := csv.NewReader(f)
	csvReader.LazyQuotes = true

	Import := new(importsCtrl.ImportsAPI)
	warehouseObj := new(models.Warehouse)
	warehouse, err := warehouseObj.GetById(importObject.Warehouse.Hex())
	if err != nil {
		return err
	}
	headersMap := map[string]string{}
	if warehouse.PrefixSuffixText == "ROM-" || warehouse.PrefixSuffixText == "fdsa-" {
		headersMap = Import.GetRomMap()
	} else if warehouse.PrefixSuffixText == "PM-" {
		headersMap = Import.GetProperMap()
	}
	settings := helpers.CsvSettings{
		Offset:    1,
		HeaderMap: headersMap,
		Headers:   true,
	}
	header := []string{}
	i := 0
	n := 0
	b := Db.C("products").Bulk()
	b.Unordered()
	for {
		rows, err := csvReader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Printf("File error: %s", err)
			return err
		}
		//fmt.Printf("%+v\n", rows)
		if i == 0 {
			header = rows
		}
		if i > settings.Offset {
			//fmt.Printf("%+v\n", header)
			row := helpers.HeaderToData(rows, header, settings)
			//fmt.Printf("%+v\n", row)

			selector := bson.M{}
			if importObject.Warehouse == "" {
				selector = bson.M{
					"suppliers.supplier_id": importObject.Supplier,
					"barcode": sanitizeAndTrim(row["barcode"]),
				}
			} else if importObject.Supplier == "" {
				selector = bson.M{
					"warehouses.warehouse_id": importObject.Warehouse,
					"barcode": sanitizeAndTrim(row["barcode"]),
				}
			} else if importObject.Warehouse == "" && importObject.Supplier == "" {
				err = errors.New("Please select warehouse or supplier or both")
				return err
			} else {
				selector = bson.M{
					"warehouses.warehouse_id": importObject.Warehouse,
					"suppliers.supplier_id":   importObject.Supplier,
					"barcode": sanitizeAndTrim(row["barcode"]),
				}
			}

			b.Remove(selector)
			n++
			if n == 99 {
				_, err = b.Run()
				if err != nil {
					log.Printf("bulk.Run: %s", err)
					return err
				}
				b = Db.C("products").Bulk()
				b.Unordered()
				n = 0
			}
		}
		i++
	}

	if n > 0 {
		_, err := b.Run()
		if err != nil {
			log.Printf("bulk.Run: %s", err)
			return err
		}
	}
	return nil
}

func modifyPrice(price string, modifier float64) string {
	var cost string
	if modifier != 0 {
		origPrice, _ := decimal.NewFromString(price)
		priceModifier := decimal.NewFromFloat(modifier)
		priceModified := origPrice.Mul(priceModifier)
		priceDiff := priceModified.Div(decimal.NewFromFloat(100))
		cost = origPrice.Add(priceDiff).Round(2).String()
	} else {
		cost = price
	}
	return cost
}

func modifyQty(qty string, modifier float64) string {
	var stock string
	if modifier != 0 {
		origQty, _ := decimal.NewFromString(qty)
		qtyLimit := decimal.NewFromFloat(modifier)
		qtyModified := origQty.Mul(qtyLimit)
		newQty := qtyModified.Div(decimal.NewFromFloat(100))
		stock = newQty.Round(0).String()
	} else {
		stock = qty
	}
	return stock
}

func sanitizeAndTrim(str string) string {
	return bluemonday.UGCPolicy().Sanitize(strings.TrimSpace(str))
}

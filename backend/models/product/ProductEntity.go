package product

import (
	"github.com/shopspring/decimal"
	"time"
	"gopkg.in/mgo.v2/bson"
)

type ProductEntity struct {
	Id          bson.ObjectId `conform:"trim" json:"id" bson:"_id,omitempty"`
	Name        string `conform:"trim" json:"name" bson:"name"`
	Status      bool `conform:"trim" json:"status" bson:"status"`
	CreatedAt   time.Time `conform:"trim" json:"created_at" bson:"created_at"`
	UpdatedAt   time.Time `conform:"trim" json:"updated_at,omitempty" bson:"updated_at"`
	Attributes  []Attribute `conform:"trim" json:"attributes,omitempty" bson:"attributes"`
	//Cost        []Cost `conform:"trim" json:"cost" bson:"cost"`
	//Price       []Cost `conform:"trim" json:"price" bson:"price"`
	//Qty         decimal.Decimal `conform:"trim" json:"qty" bson:"qty"`
	Description string `conform:"trim" json:"description" bson:"description"`
	Weight      decimal.Decimal `conform:"trim" json:"weight" bson:"weight"`
	Suppliers   []SupplierData `json:"suppliers" bson:"suppliers"`
	Warehouses  []WarehouseData `json:"warehouses" bson:"warehouses"`
	EanBarcode  string `conform:"trim" json:"ean_barcode" bson:"ean_barcode"`
	UpcBarcode  string `conform:"trim" json:"upc_barcode" bson:"upc_barcode"`
	//Marketplaces []Mp `json:"marketplace,omitempty" bson:"marketplace"`
	//AccountId   bson.ObjectId `conform:"trim" json:"account_id,omitempty" bson: "account_id"` // Assigned to company
	//UserId      bson.ObjectId `conform:"trim" json:"user_id,omitempty" bson: "user_id"`   // Created by user
	//Formula      []bson.ObjectId `conform:"trim" json:"formula,omitempty" bson:"formula"`
}
type Attribute struct {
	Code   bson.ObjectId `conform:"trim" json:"code" bson:"code"`
	Values []bson.ObjectId `bson:"values"`
}
type Cost struct {
	Price    decimal.Decimal `conform:"trim" bson:"current"` // Current price
	Currency string `conform:"trim" bson:"currency"`
}
// Marketplace
type Mp struct {
	Id          bson.ObjectId `conform:"trim" bson: "_id"`
	Unique      bool `conform:"trim" bson:"unique"`      // Is product unique on marketplace?
	Price       []Cost `conform:"trim" bson:"new_price"` // Price after repricing
	Competitors []Competitor `conform:"trim" bson:"competitors"`
}
type Competitor struct {
	Url     string `conform:"trim" bson:"url"`
	Country string `conform:"trim" bson:"country"`
	Cost
}
type SupplierData struct {
	SupplierId bson.ObjectId `conform:"trim" json:"supplier_id" bson:"supplier_id"`
	Qty        decimal.Decimal `conform:"trim" json:"qty" bson:"qty"`
	Sku        string `conform:"trim" json:"sku" bson:"sku"`
	Cost
}
type WarehouseData struct {
	WarehouseId bson.ObjectId `conform:"trim" json:"warehouse_id" bson:"warehouse_id"`
	Qty         decimal.Decimal `conform:"trim" json:"qty" bson:"qty"`
	Sku         string `conform:"trim" json:"sku" bson:"sku"`
	Cost
}
package productAttributes

import (
	"gopkg.in/mgo.v2/bson"
)

type ProductAttributesValuesEntity struct {
	Id          bson.ObjectId `json:"id" bson:"_id,omitempty"`
	AttributeId bson.ObjectId `json:"attribute_id" bson:"attribute_id"`
	Option      string `conform:"trim" json:"option" bson:"option"`
}


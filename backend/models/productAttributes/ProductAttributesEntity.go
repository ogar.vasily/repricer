package productAttributes

import "gopkg.in/mgo.v2/bson"

type ProductAttributesEntity struct {
	Id             bson.ObjectId `conform:"trim" json:"id" bson:"_id,omitempty"`
	Name           string `conform:"trim" json:"name" bson:"name"`
	Code           string `conform:"trim" json:"code" bson:"code"` // For import with automapping, for datatables
	Position       uint8 `conform:"trim" json:"position,string" bson:"position"`
	IsColumn       bool `conform:"trim" json:"is_column" bson:"is_column"`
	ColumnPosition uint8 `conform:"trim" json:"column_position,string" bson:"column_position"`
	Type           string `conform:"trim" json:"type" bson:"type"` // Date, Number, Text, Select
	System         bool `conform:"trim" json:"system" bson:"system"`
								       //UserId         bson.ObjectId `bson: "user_id"`
}

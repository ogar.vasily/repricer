package productAttributes

import (
	"gopkg.in/mgo.v2/bson"
	"repr/backend/db"
)

func getValueById(id bson.ObjectId) (ProductAttributesValuesEntity, error) {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	result := ProductAttributesValuesEntity{}
	err := Db.C("product_attributes_values").Find(bson.M{"_id": id}).One(&result)
	if err != nil {
		return result, err
	} else {
		return result, nil
	}
}

func UpdateById(id bson.ObjectId, data string) error {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()
	//log.Println(data)
	// Update
	err := Db.C("product_attributes_values").Update(bson.M{"_id": id}, bson.M{"option": data})
	if err != nil {
		return err
	} else {
		return nil
	}
}

func GetAllByAttributeId(attributeId string) ([]ProductAttributesValuesEntity, error) {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	result := []ProductAttributesValuesEntity{}
	err := Db.C("product_attributes_values").Find(bson.M{"attribute_id": bson.ObjectIdHex(attributeId)}).All(&result)
	if err != nil {
		return nil, err
	} else {
		return result, nil
	}
}

func CreateAttributeValue(attribute ProductAttributesValuesEntity) (ProductAttributesValuesEntity, error) {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()
	var err error

	if attribute.Id == "" {
		attribute.Id = bson.NewObjectId()
		err = Db.C("product_attributes_values").Insert(attribute)
	} else {
		value := bson.M{"option": attribute.Option, "attribute_id": attribute.AttributeId}
		err = Db.C("product_attributes_values").Update(bson.M{"_id": attribute.Id}, value)
	}
	return attribute, err
}

func DeleteById(id string) error {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()
	err := Db.C("product_attributes_values").Remove(bson.M{"_id": bson.ObjectIdHex(id)})
	err = Db.C("products").Update(bson.M{"attributes.$.values": id}, bson.M{})
	return err
}
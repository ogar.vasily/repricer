package productAttributes

import (
	"repr/backend/db"
	"gopkg.in/mgo.v2/bson"
	"fmt"
)

type ProductAttributes struct {
}

func GetProductColumns() ([]ProductAttributesEntity, error) {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	var result []ProductAttributesEntity
	err := Db.C("product_attributes").Find(bson.M{"is_column":true}).Sort("column_position").All(&result)
	if err != nil {
		return nil, err
	} else {
		return result, nil
	}
}

func GetAllProductAttrs() ([]bson.M, error) {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	p1 := bson.M{"$match":bson.M{}}
	p2 := bson.M{"$lookup": bson.M{
		"from": "product_attributes_values",
		"localField": "_id",
		"foreignField": "attribute_id",
		"as": "options"},
	}
	p5 := bson.M{"$project": bson.M{
		"_id": 1,
		"code": 1,
		"name": 1,
		"type": 1,
		"options": 1,
		"system": 1,
		"is_column": 1,
		"position": 1,
		"column_position": 1,
	}}
	var pipeline []bson.M
	pipeline = []bson.M{p1, p2, p5}

	result := []bson.M{}
	cursor := Db.C("product_attributes").Pipe(pipeline).Iter()
	err := cursor.All(&result)

	if err != nil {
		return nil, err
	} else {
		return result, nil
	}
}

// Get product attributes by id (GET)
func GetProductAttrById(id string) (ProductAttributesEntity, error) {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	result := ProductAttributesEntity{}
	err := Db.C("product_attributes").Find(bson.M{"_id": bson.ObjectIdHex(id)}).One(&result)
	return result, err
}

// Update product attributes (PUT) /product-attributes/update/:id
func UpdateProductAttrById(id string, attributes *ProductAttributesEntity) error {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	err := Db.C("product_attributes").Update(bson.M{"_id": bson.ObjectIdHex(id)}, &attributes)
	return err
}

// Create product attributes (POST) /product-attributes/create
func CreateProductAttr(attributes *ProductAttributesEntity) error {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	if attributes.IsColumn == true {
		index := "attributes." + attributes.Name
		fmt.Println(index)
		Db.C("products").EnsureIndexKey(index)
	}

	err := Db.C("product_attributes").Insert(&attributes)
	return err
}

//Delete product attributes by id (DELETE) /product-attributes/:id
func DeleteProductAttrById(id string) error {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()
	result := ProductAttributesEntity{}
	err := Db.C("product_attributes").Find(bson.M{"_id": bson.ObjectIdHex(id)}).One(&result)
	if err != nil {
		return err
	} else {
		if result.Type == "select" || result.Type == "multiselect" {
			err = Db.C("product_attributes_values").Remove(bson.M{"attribute_id": bson.ObjectIdHex(id)})
			err = Db.C("products").Update(bson.M{"attributes.$.values": id}, bson.M{})
		} else {
			selector := bson.M{"attributes." + result.Code: bson.M{"$exists": true}}
			modifier := bson.M{"$unset": bson.M{"attributes." + result.Code: ""}}
			_, err = Db.C("products").UpdateAll(selector, modifier)
		}
	}
	err = Db.C("product_attributes").Remove(bson.M{"_id": bson.ObjectIdHex(id)})
	return err
}
package models

import (
	"gopkg.in/mgo.v2/bson"
	"repr/backend/db"
)

type Warehouse struct {
	Id               bson.ObjectId `conform:"trim" json:"id" bson:"_id,omitempty"`
	CompanyName      string `conform:"trim" json:"name" bson:"name"`
	Address          Address `json:"address" bson:"address"`
	Email            string `conform:"trim" json:"email" bson:"email,omitempty"`
	Phone            string `conform:"trim" json:"phone" bson:"phone,omitempty"`
	Fax              string `conform:"trim" json:"fax" bson:"fax"`
	VAT              string `conform:"trim" json:"vat_number" bson:"vat_number"`
	Status           bool `conform:"trim" json:"status" bson:"status"`
	Primary          bool `conform:"trim" json:"primary" bson:"primary"`
	PrefixSuffix     string `conform:"trim" json:"prefix_suffix" bson:"prefix_suffix,omitempty"`
	PrefixSuffixText string `conform:"trim" json:"prefix_suffix_text" bson:"prefix_suffix_text,omitempty"`
}

func (this *Warehouse) GetAll() ([]interface{}, error) {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	var result []interface{}
	if err := Db.C("warehouses").Find(nil).All(&result); err != nil {
		return nil, err
	} else {
		return result, nil
	}
}
func (this *Warehouse) GetAllProjected(fields bson.M) ([]bson.M, error) {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	var result []bson.M
	if err := Db.C("warehouses").Find(nil).Select(fields).All(&result); err != nil {
		return nil, err
	} else {
		return result, nil
	}
}

func (this *Warehouse) GetById(id string) (Warehouse, error) {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	result := Warehouse{}
	if err := Db.C("warehouses").Find(bson.M{"_id": bson.ObjectIdHex(id)}).One(&result); err != nil {
		return result, err
	} else {
		return result, nil
	}
}

func (this *Warehouse) UpdateById(id string, data interface{}) (Warehouse, error) {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	// Update
	result := Warehouse{}
	if err := Db.C("warehouses").Update(bson.M{"_id": bson.ObjectIdHex(id)}, &data); err != nil {
		return result, err
	} else {
		return result, nil
	}
}

func (this *Warehouse) Create(supplier *Warehouse) error {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	if err := Db.C("warehouses").Insert(&supplier); err != nil {
		return err
	} else {
		return nil
	}
}

func (this *Warehouse) Delete(id string) error {
	Db := db.MgoDb{}
	Db.Init()
	defer Db.Close()

	if err := Db.C("warehouses").Remove(bson.M{"_id": bson.ObjectIdHex(id)}); err != nil {
		return err
	} else {
		return nil
	}
}

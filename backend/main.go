package main

import (
	"gopkg.in/gin-gonic/gin.v1"
	"repr/backend/api"
	"repr/backend/db"
	"repr/backend/api/productCtrl"
	"repr/backend/api/productAttributesCtrl"
	//"repr/backend/helpers"
	"repr/backend/api/importsCtrl"
	"repr/backend/routes"
)

func main() {
	router := gin.New()

	// set the favicon
	router.StaticFile("/favicon.ico","../frontend/public/images/favicon.ico")
	router.Static("/public", "../frontend/public")
	router.LoadHTMLGlob("../frontend/templates/*")

	// DB Main
	DbMain()

	router.GET("/", routes.Index)

	v1 := router.Group("/api/v1")
	{
		// Products handler
		products := new(productCtrl.ProductAPI)
		product := v1.Group("/products")
		{
			product.POST("/create", products.CreateProduct)
			product.GET("/", products.GetAllProducts)
			product.GET("product/:id", products.GetProductById)
			product.PUT("/update/:id", products.UpdateProductById)
			product.DELETE("/delete/:id", products.DeleteProductById)
			product.GET("/cron/:id", products.UpdateProducts)
		}

		// General settings
		settings := new(api.SettingsAPI)
		v1.GET("/settings/:name", settings.GetSettingsByName)

		// Product attributes
		productAttr := new(productAttributesCtrl.ProductAttrAPI)
		//helper := new(helpers.Helpers)
		//helper.Sanitize(data)
		attributes := v1.Group("/product-attributes")
		{
			attributes.POST("/create", productAttr.CreateProductAttr)
			attributes.GET("/", productAttr.GetAllProductAttrs)
			attributes.GET("/attribute/:id", productAttr.GetProductAttrById)
			attributes.GET("/aggregated", productAttr.GetProductAttrAggregated)
			attributes.PUT("/update/:id", productAttr.UpdateProductAttrById)
			attributes.DELETE("/delete/:id", productAttr.DeleteProductAttrById)
			attributes.GET("/get-columns", productAttr.GetProductColumns)
		}

		// Product attribute values
		productAttrValues := new(productAttributesCtrl.ProductAttrValuesAPI)
		prodAttrValues := v1.Group("/product-attribute-values")
		{
			prodAttrValues.POST("/create", productAttrValues.CreateProductAttrValues)
			prodAttrValues.GET("/:attribute_id", productAttrValues.GetProductAttrValues)
			prodAttrValues.DELETE("/delete/:id", productAttrValues.DeleteProductAttrValue)
		}

		// Imports
		imports := new(importsCtrl.ImportsAPI)
		importsR := v1.Group("/imports")
		{
			importsR.POST("/create", imports.CreateImport)
			importsR.GET("/", imports.GetAllImports)
			importsR.GET("/import/:id", imports.GetImportById)
			importsR.PUT("/update/:id", imports.UpdateImportById)
			importsR.DELETE("/delete/:id", imports.DeleteImportById)

			importsR.GET("/csvData", imports.GetCsvData)
		}

		// Suppliers
		suppliers := new(api.SuppliersAPI)
		supplier := v1.Group("/suppliers")
		{
			supplier.POST("/create", suppliers.CreateSupplier)
			supplier.GET("/", suppliers.GetAllSuppliers)
			supplier.GET("/fields/:fields", suppliers.GetAllSuppliersFields)
			supplier.GET("/supplier/:id", suppliers.GetSupplierById)
			supplier.PUT("/update/:id", suppliers.UpdateSupplierById)
			supplier.DELETE("/delete/:id", suppliers.DeleteSupplierById)
		}

		// Warehouses
		warehouses := new(api.WarehousesAPI)
		warehouse := v1.Group("/warehouses")
		{
			warehouse.POST("/create", warehouses.CreateWarehouse)
			warehouse.GET("/", warehouses.GetAllWarehouses)
			warehouse.GET("/fields/:fields", warehouses.GetAllWarehousesFields)
			warehouse.GET("/warehouse/:id", warehouses.GetWarehouseById)
			warehouse.PUT("/update/:id", warehouses.UpdateWarehouseById)
			warehouse.DELETE("/delete/:id", warehouses.DeleteWarehouseById)
		}
	}
	router.Run(":8080")
}

func DbMain() {
	// Database Main Connection
	Db := db.MgoDb{}
	Db.Init()
	// index keys
	keys := []string{"email"}
	Db.Index("auth", keys)
}

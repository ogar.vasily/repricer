package routes

import (
	"gopkg.in/gin-gonic/gin.v1"
	"net/http"
)

// Index index is the page for GET: / route
func Index(ctx *gin.Context) {
	index := gin.H{
		"Title":   "MusicSeller sample - index",
		"Message": "This is just a sample index",
	}
	ctx.HTML(http.StatusOK, "layout.html", index)
}
